import { Agent as HttpsAgent } from 'https';
import axios, { AxiosRequestConfig } from 'axios';
import { doesVersionsListSatisfyRange } from './semver-utils';
import { Logger } from './models/logger';


async function fetchPackageVersions(packageName: string, registryUrl: string, useSsl: boolean = true, logger: Logger = null): Promise<string[]> {
    return new Promise((resolve, reject) => {
        let queryUrl = `${registryUrl}/${packageName}`;

        let options: AxiosRequestConfig = {};
        if (!useSsl) options.httpsAgent = new HttpsAgent({rejectUnauthorized: false});

        logger && logger.debug(`Querying url: '${queryUrl}'`);
        axios.get(queryUrl, options)
            .then(res => {
                let versions = Object.keys(res.data.versions);
                logger && logger.debug(`Versions for package '${packageName}': ${JSON.stringify(versions)}`);
                resolve(versions);
            })
            .catch(err => {
                if (err.response && err.response.status === 404) {
                    logger && logger.debug(`Received 404 for package '${packageName}'`);
                    resolve([]);
                }
                else {
                    logger && logger.error(`Error occured for package '${packageName}' with url '${queryUrl}'`);
                    reject(err);
                }
            });
    });
}

export async function doesRegistrySatisfyVersion(packageName: string, requiredVersionRange: string, registryUrl: string, useSsl: boolean = true, logger: Logger = null): Promise<boolean> {
    return fetchPackageVersions(packageName, registryUrl, useSsl, logger)
        .then(versions => doesVersionsListSatisfyRange(requiredVersionRange, versions));
}
