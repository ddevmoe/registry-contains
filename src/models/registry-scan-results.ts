interface RegistryScanResults {
    existing: string[],
    missing: string[]
}