export interface PackageSemver {
    name: string,
    version: string
}
