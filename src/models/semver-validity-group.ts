interface SemverValidityGroup {
    valid: string[],
    invalid: string[]
}