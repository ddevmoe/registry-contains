import * as semver from 'semver';
import { PackageSemver } from './models/package-semver';


export function matchingVersionsByRange(versionRange: string, existingVersions: string[]): string[] {
    return existingVersions.filter((version, index, _) => semver.satisfies(version, versionRange));
}

export function doesVersionsListSatisfyRange(versionRange: string, existingVersions: string[]): boolean {
    return !!matchingVersionsByRange(versionRange, existingVersions).length;
}

export function spreadPackageSemver(packageSemver: string): PackageSemver {
    const result: PackageSemver = {
        name: '',
        version: ''
    };
    let splitted = packageSemver.split('@');

    if (splitted.length === 1) result.name = packageSemver;
    else if (packageSemver.startsWith('@') && splitted.length < 3) {
        result.name = packageSemver;
    }
    else {
        result.version = splitted.pop();
        result.name = splitted.join('@');
    }

    return result;
}

export function groupPackagesBySemverRangeValidity(packagesSemverRanges: string[]): SemverValidityGroup {
    let result: SemverValidityGroup = {
        valid: [],
        invalid: []
    };

    packagesSemverRanges.forEach(range => {
        let { version } = spreadPackageSemver(range);
        if (!version || isValidSemverRange(version)) result.valid.push(range);
        else result.invalid.push(range);
    });

    return result;
}

export const isValidSemverRange = semver.validRange;
