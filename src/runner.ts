import { doesRegistrySatisfyVersion } from './registry-acess';
import { spreadPackageSemver } from './semver-utils';
import { Logger } from './models/logger';


export async function scanRegistryForVersions(packagesSemvers: string[], registry: string, useSsl: boolean = true, logger: Logger = null): Promise<RegistryScanResults> {
    const scanResults: RegistryScanResults = {
        existing: [],
        missing: []
    };

    return Promise.all(packagesSemvers.map(packageSemver => {
        let { name, version } = spreadPackageSemver(packageSemver);
        return doesRegistrySatisfyVersion(name, version, registry, useSsl, logger)
                .then(exists => ({ packageSemver, exists }));
    }))
    .then(results => {
        results.forEach(r => {
            if (r.exists) scanResults.existing.push(r.packageSemver);
            else scanResults.missing.push(r.packageSemver);
        });
        return scanResults;
    });
}

export function extractDependenciesFromPackageJson(packageJsonPath: string, includeDev: boolean = false): string[] {
    const packageJsonContents = require(packageJsonPath);
    let dependencies = packageJsonContents.dependencies;
    let packages = [];

    for (let d in dependencies) {
        packages.push(`${d}@${dependencies[d]}`);
    }

    if (includeDev) {
        let devDependencies = packageJsonContents.devDependencies || {};
        for (let d in devDependencies) {
            packages.push(`${d}@${devDependencies[d]}`);
        }
    }

    return packages;
}
