import * as winston from 'winston';


export function initialize(verbose: string = 'debug', useColors: boolean = true) {
    winston.configure({
        level: verbose,
        transports: [
            new winston.transports.Console({
                format: winston.format.combine(
                    winston.format.colorize({
                        all: useColors,
                    }),
                    winston.format.printf((info): string => {
                        return info.message;
                    })
                )
            })
        ]
    });
}
