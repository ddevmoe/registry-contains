import * as program from 'commander';
import * as path from 'path';
import * as fs from 'fs';
import { exec } from 'child_process';
import * as logger from 'winston';

import { scanRegistryForVersions, extractDependenciesFromPackageJson } from './runner';
import { groupPackagesBySemverRangeValidity } from './semver-utils';
import { initialize as initializerLogger } from './logger';


async function getGlboalRegistry(): Promise<string> {
    return new Promise((resolve, reject) => {
        exec('npm get registry', (err, output) => {
            if (err) {
                logger && logger.error(`Failed fetching global registry`);
                reject(err);
            }
            else {
                let registry = output.slice(0, -1);
                logger && logger.debug(`Using global npm registry '${registry}'`);
                resolve(registry);
            }
        });
    });
}

function normalizeUrl(url: string): string {
    let normalizedUrl = url.replace(/\/+$/, '');
    logger && logger.debug(`Normalized url to '${normalizedUrl}'`);

    return normalizedUrl;
}

function parsePackages(packagesNames: string[], registry: string, ssl: boolean) {
    let validityGroup = groupPackagesBySemverRangeValidity(packagesNames);
    validityGroup.invalid.forEach(packageRange =>
        logger && logger.warn(`Provided package '${packageRange}' could not be parsed as a valid semver range.`));

    scanRegistryForVersions(validityGroup.valid, registry, ssl, logger)
        .then(results => {
            if (!results.missing.length) {
                logger && logger.info(`You're in luck! All [${results.existing.length}] packages have an existing version match.`);
            }
            else {
                logger && logger.info(`Total of [${results.missing.length}] packages are missing:`);
                results.missing.sort().forEach((packageName, index) => {
                    logger && logger.warn(`${index + 1}) ${packageName}`);
                });
            }
        })
        .catch(err => {
            logger && logger.error(err.message);
        });
}

function initLogger(isVerbose: boolean, forceColors: boolean) {
    initializerLogger(isVerbose ? 'debug' : 'info', process.stdout.isTTY || forceColors || false);
}

async function main() {
    const packageJson = require('../package.json');
    const { version } = packageJson;

    program
        .version(version)
        .description('verify version requirements against a registry')
        .option('-r, --registry [registry]', 'registry url to verify against')
        .option('--no-ssl', 'whether or not to force use of ssl')
        .option('--verbose', 'set to verbose mode')
        .option('--force-colors', 'use colors even when being piped');

    program
        .command('packages [packages...]')
        .description('verify specific package')
        .action(async (packagesNames: string[], cmd) => {
            initLogger(cmd.parent.verbose, cmd.parent.forceColors);
            
            let registry = normalizeUrl(cmd.parent.registry || await getGlboalRegistry());
            parsePackages(packagesNames, registry, cmd.parent.ssl);
        });

    program
        .command('package-json <path>')
        .description('verify dependencies requirements from a package.json file')
        .option('--include-dev', 'include dev-dependencies as well')
        .action(async (filePath: string, cmd) => {
            initializerLogger(cmd.parent.verbose, cmd.parent.forceColors);

            let absolutePath = path.resolve(filePath);
            if (!fs.existsSync(absolutePath)) {
                logger && logger.error(`Provided path "${absolutePath}" does not exist!`);
                process.exit(-1);
            }

            let packages = extractDependenciesFromPackageJson(absolutePath, cmd.includeDev);
            let registry = normalizeUrl(cmd.parent.registry || await getGlboalRegistry());
            parsePackages(packages, registry, cmd.parent.ssl);
        });

    if (process.argv.length < 3 || !program.commands.map(c => c.name()).includes(process.argv[2]))
        program.help();
    else
        program.parse(process.argv);
};

main();
