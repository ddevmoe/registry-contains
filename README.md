# Registry Contains
Registry Contains is a CLI tool that helps you identify which packages are missing in an npm-registry.

Aimed at assisting nodejs development within air-gapped / isolated networks using an inner npm mirror / registry.


## Seems rather <i>useless</i> to me... What am I missing?
Let us imagine the following case:

You're a node developer happily working a nice cyber-security company that - for security measures - has it's own private network.

You just copied an example app you'd like to learn from into the network. It requires some dependencies so you hit an 'npm i' into a shell of your choice when... Oops! \<some-package> is missing in your private npm registry. You take a deep breathe and ask the IT guys to get you that missing piece of data into the network (which can sometimes... be a little frustrating, to say the least).

Alright, after some testing and scanning they get your package inside the network, you upload it to your registry and hit 'npm i' once again.

...\<some-other-package> is missing. Aahhhh...

#### Ever happened to you?
Probably not. But if you're one of a few - this package may be of assistance to you.

    $ registry-contains package-json /path/to/app/package.json
    Total of [3] packages are missing:
    1) omg-what@^1.5.0
    2) ok-buddy@2.1.1
    3) @types/name-name@0.34.0

## Usage:

### Using <b>packages</b> command:
##### Syntax: registry-contains packages [package@version package . . .]

```
$ registry-contains packages awesome-package cool-package@3.9.0 omg-what@0.20.1
```

### Using  <b>package-json</b> command:
##### Syntax: registry-contains package-json /path/to/app/package.json

```
$ registry-contains package-json ./package.json
```

## Options

- ### ```--registry <url>```
    Use the specified registry instead of the configured one (default would be the same as <b>'npm get registry'</b>)

- ### ```--no-ssl```
    Disable CA verification

### Run ```"registry-contains --help"``` for list of commands / options